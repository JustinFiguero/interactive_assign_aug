#pragma once
#include <iostream>
#include <string>;

using std::cout;
using std::cin;
using std::endl;
using std::string;

//The function prototypes, these name the functions and their variables.
int GetTroop();
int GetKill();
string GetLead();
int Intro(int Tr, int Kia);
void Begin(int Tr, string lead);
void Mid(int Kia, int Surv, string lead);
void End(string lead);
