// Creator: Justin Figuero
// jusfigue@uat.edu
// Interactive assignment
// Personalized tale
// 7/17/2021

#include <iostream>
#include <string>;
#include "Header.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
	// calling all functions that will be used to build our story.
	int troopers = 1, killed = 1, survivors = 1;
	string leader;
	troopers = GetTroop();
	killed = GetKill();
	leader = GetLead();
	survivors = Intro(troopers, killed);

	// defining the variables that will be in the functions
	Begin(troopers, leader);
	Mid( killed, survivors, leader);
	End( leader);
	
	return 0;
}
