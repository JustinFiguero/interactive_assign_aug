#include "Header.h"

int GetTroop() {
	// reciving input from player for troop number.
	cout << "Please enter the following for your personalized adventure\n";
	int Tr;
	cout << "Enter a number: ";
	cin >> Tr;
	return Tr;
}
int GetKill() {
	// reciving input from player for troops killed.
	int Kia;
	cout << "Enter a number, smaller than the first: ";
	cin >> Kia;
	return Kia;

}
string GetLead() {
	// reciving a name from the player. 
	string lead;
	cout << "Enter your last name: ";
	cin >> lead;
	return lead;
}


int Intro(int Tr, int Kia) {
	// defining the survivor function and welcoming the player.

	cout << "Welcome to Rakoto,\n\n";

	int Surv = Tr - Kia;

	return Surv;
}
void Begin(int Tr, string lead) {
	//beginning the story and displaying the results of the Tr and lead variables.

	cout << "\nA platoon of " << Tr << " are forced to parachute off their plane after taking heavy fire \n";
	cout << "-- the plane was going down, with little choice they drop feet first into Hell. \n";
	cout << "The commander was shot on the way down, the second in command steps up to the plate, Captain " << lead << ".\n";
}
void Mid(int Kia, int Surv, string lead) {
	//contuining the story and showing the Kia surv and lead variables.

	cout << "\nAfter landing, the troopers contacted HQ. While speaking to HQ the crew got ambushed by the enemy. \n";
	cout << "Bullets flew by the crew as they took cover, once the firing stopped " << lead;
	cout << ", ordered a return fire. The enemies met their swift end but not before claiming some good lives.. \n";
	cout << "Of the troopers, " << Kia << " were flatlined, ";
	cout << "leaving just " << Surv << " in the group.\n";
}
void End(string lead) {
	// ending the story with the lead variable.

	cout << "\nThe troops were convinced they would die here. \n";
	cout << "But while tending to the wounded, ";
	cout << "a whirring sound grazed them, it was the evac helicopter crew from HQ. \n";
	cout << "the troopers climbed aboard and returned home\n";
	cout << lead << " got promoted to commander due to the exmeplary bravery shown on the battlefield.\n";
}